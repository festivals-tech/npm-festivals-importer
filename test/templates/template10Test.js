var chai = require('chai');
var expect = chai.expect;
var should = chai.should();

describe('template10 importer test', function () {

  var template10 = require('../../lib/templates/template10');

  it('should import to file', function (done) {
    template10.handle(null, function (err, result) {
        console.log(result);
        done();
      }
    );
  });

  // 1909

  // it('should import', function (done) {
  //   var body = '<div class="main"> <div class="page-heading page-heading--big page-heading--with-gray-bg"> <h1>Każda świątynia stoi na krwi</h1> </div> <div class="main-content"> <div class="program-point"> <div class="program-point__details"> <div class="table-responsive"> <table> <tbody> <tr> <th>Termin</th> <td>Pt, 28 kwietnia, 15:00</td> </tr> <tr> <th>Czas trwania prelekcji</th> <td>50&nbsp;minut</td> </tr> <tr> <th>Sala/miejsce</th> <td>Literacka 2</td> </tr> <tr> <th>Kategoria</th> <td>Blok Literacki</td> </tr> <tr> <th>Opis</th> <td>O tym, jak z niczego powstawały fortuny w latach 90.Tło fabularne do "Moskala" i zarys sytuacji, która zamieniła upadający ZSRR we wznoszące się imperium rosyjskie. Jak na ruinach komunizmu, pośród wielonarodowościowego gwaru stadionowych bazarów i w zaciszu viproomów pierwszych klubów nocnych wyrasta fortuna, zdolna złamać duszę.Znajdziesz się w przedsionku prywatnego piekła.https://www.youtube.com/watch?v=9DurIiXF3eY</td> </tr> <tr> <th>Prowadzący</th> <td> <ul> <li>Michał "Meesh" Gołkowski</li> </ul> </td> </tr> <tr> <th>Tagi</th> <td> <ul> <a href="/2017/program/?tag=golkowski_1" class="button button--small">#Gołkowski</a>&nbsp; <a href="/2017/program/?tag=moskal" class="button button--small">#Moskal</a>&nbsp; <a href="/2017/program/?tag=fabrykaslow" class="button button--small">#FABRYKASŁÓW</a>&nbsp; </ul> </td> </tr> </tbody> </table> </div> </div> <!-- <div class="program-point__action"> <a href="/2017/program/" title="" class="button button--icon-arrow-right button--icon-right">Wróć do programu</a> </div> --></div> </div> </div>';
  //   template10.parseEvent(1909, body, function (err, result) {
  //       console.log(result);
  //       result.should.have.deep.property('name', 'Każda świątynia stoi na krwi');
  //       result.should.have.deep.property('description', 'O tym, jak z niczego powstawały fortuny w latach 90.Tło fabularne do "Moskala" i zarys sytuacji, która zamieniła upadający ZSRR we wznoszące się imperium rosyjskie. Jak na ruinach komunizmu, pośród wielonarodowościowego gwaru stadionowych bazarów i w zaciszu viproomów pierwszych klubów nocnych wyrasta fortuna, zdolna złamać duszę.Znajdziesz się w przedsionku prywatnego piekła.https://www.youtube.com/watch?v=9DurIiXF3eY');
  //       result.should.have.deep.property('duration.startAt', '2017-04-28T13:00:00.000Z');
  //       result.should.have.deep.property('duration.finishAt', '2017-04-28T13:50:00.000Z');
  //       result.should.have.deep.property('tags[0]', '1909');
  //       result.should.have.deep.property('tags[1]', '#Gołkowski');
  //       result.should.have.deep.property('tags[2]', '#Moskal');
  //       result.should.have.deep.property('tags[3]', '#FABRYKASŁÓW');
  //       result.should.have.deep.property('authors[0].name', 'Michał "Meesh" Gołkowski');
  //       result.should.have.deep.property('categories[0].name', 'Blok Literacki');
  //       result.should.have.deep.property('places[0].name', 'Literacka 2');
  //
  //       done();
  //     }
  //   );
  // });

  // it('should import null start date', function (done) {
  //   var body = '<div class="container"> <div class="main"> <div class="page-heading page-heading--big page-heading--with-gray-bg"> <h1>Magia fizyki (piątek od 15 o każdej pełnej godz.)</h1> </div> <div class="main-content"> <div class="program-point"> <div class="program-point__details"> <div class="table-responsive"> <table> <tbody> <tr> <th>Termin</th> <td></td> </tr> <tr> <th>Czas trwania prelekcji</th> <td>4&nbsp;godziny, 50&nbsp;minut</td> </tr> <tr> <th>Sala/miejsce</th> <td>Świetlica Fantastyczna</td> </tr> <tr> <th>Kategoria</th> <td>Blok Dziecięcy</td> </tr> <tr> <th>Opis</th> <td>Witamy w fantastycznym świecie fizyki! Zademonstrujemy Wam ciekawe doświadczenia. W programie między innymi: opowiemy wam czym są ruchome piaski, zademonstrujemy zdumiewające złudzenia optyczne oraz odwiedzimy mroźną krainę ciekłego azotu. Udowodnimy, że fizyka wcale nie musi być nudna! Chcecie wiedzieć więcej? Serdecznie zapraszamy!Początek o każdej pełnej godzinie w piątek, czas trwania ok 45 min.</td> </tr> <tr> <th>Prowadzący</th> <td> <ul> <li>Ignacy "SKNFFusion" Zgrajek</li> </ul> </td> </tr> <tr> <th>Tagi</th> <td> <ul> <a href="/2017/program/?tag=dla-dzieci" class="button button--small">dla dzieci</a>&nbsp; <a href="/2017/program/?tag=doswiadczenia_1" class="button button--small">Doświadczenia</a>&nbsp; <a href="/2017/program/?tag=eng" class="button button--small">eng</a>&nbsp; <a href="/2017/program/?tag=rodzinny-pyrkon" class="button button--small">rodzinny Pyrkon</a>&nbsp; <a href="/2017/program/?tag=fizyka" class="button button--small">fizyka</a>&nbsp; <a href="/2017/program/?tag=nauka" class="button button--small">nauka</a>&nbsp; </ul> </td> </tr> </tbody> </table> </div> </div> <!-- <div class="program-point__action"> <a href="/2017/program/" title="" class="button button--icon-arrow-right button--icon-right">Wróć do programu</a> </div> --></div> </div> </div> <div class="aside"> <div class="cta"> <a href="http://pyrkon.pl/2017/program/" class="cta__link cta__link--red"> <span>Zobacz program</span> </a> <a href="http://www.eventim.pl/festiwal-fantastyki-pyrkon-bilety.html?affiliate=PLE&amp;doc=artistPages%2Ftickets&amp;fun=artist&amp;action=tickets&amp;erid=1826261" class="cta__link cta__link--navy" target="_blank"> <span>Kup bilet</span> </a> <a href="/accounts/register/" class="cta__link cta__link--navy"> <span>Zarejestruj się</span> </a> </div> <div class="login"> <div class="login__bar"> <span>Zaloguj</span> </div> <div class="login__form"> <form class="form" method="POST" action="/accounts/login/"><input type="hidden" name="csrfmiddlewaretoken" value="mDx7CzijyUbI46QHMRws87ZQy4xAPE5k"> <div class="form__row"> <input type="text" name="username" placeholder="Adres e-mail"> </div> <div class="form__row"> <input type="password" name="password" placeholder="Hasło"> </div> <div class="form__row"> <a href="/accounts/password/reset/" class="login__lost-pass">Zapomniałeś hasła?</a> </div> <div class="form__row"> <button type="submit" class="login__button button button--icon-arrow-right button--icon-right">Logowanie</button> </div> </form> </div> </div> <div class="aside-heading"> <span>Śledź nas</span> </div> <nav class="social-aside"> <ul class="social-aside__list"> <li class="social-aside__item"> <a href="https://www.facebook.com/Pyrkon" target="_blank" class="social-aside__link icon-facebook"></a> </li> <li class="social-aside__item"> <a href="https://twitter.com/pyrkon" target="_blank" class="social-aside__link icon-twitter"></a> </li> <li class="social-aside__item"> <a href="http://www.youtube.com/user/FestiwalPyrkon" target="_blank" class="social-aside__link icon-youtube"></a> </li> <li class="social-aside__item"> <a href="https://plus.google.com/+pyrkon" target="_blank" class="social-aside__link icon-google-plus"></a> </li> </ul> </nav> </div> </div>';
  //   template10.parseEvent(1908, body, function (err, result) {
  //       console.log(result);
  //       result.should.have.deep.property('name', 'Magia fizyki (piątek od 15 o każdej pełnej godz.)');
  //       result.should.have.deep.property('description', 'Witamy w fantastycznym świecie fizyki! Zademonstrujemy Wam ciekawe doświadczenia. W programie między innymi: opowiemy wam czym są ruchome piaski, zademonstrujemy zdumiewające złudzenia optyczne oraz odwiedzimy mroźną krainę ciekłego azotu. Udowodnimy, że fizyka wcale nie musi być nudna! Chcecie wiedzieć więcej? Serdecznie zapraszamy!Początek o każdej pełnej godzinie w piątek, czas trwania ok 45 min.');
  //       result.should.have.deep.property('duration.startAt', '2016-04-08T10:00:00.000Z');
  //       result.should.have.deep.property('duration.finishAt', '2016-04-08T17:20:00.000Z');
  //       result.should.have.deep.property('tags[0]', '21');
  //       result.should.have.deep.property('authors[0].name', 'Ignacy "SKNFFusion" Zgrajek');
  //       result.should.have.deep.property('categories[0].name', 'Blok Gier Elektronicznych');
  //       result.should.have.deep.property('places[0].name', 'Scena BGE');
  //
  //       done();
  //     }
  //   );
  // });
  //
  // it('should import many autors', function (done) {
  //   var body = '<div class="main"> <div class="page-heading page-heading--big page-heading--with-gray-bg"> <h1>[Panel] Podbić świat!</h1> </div> <div class="main-content"> <div class="program-point"> <div class="program-point__details"> <div class="table-responsive"> <table> <tbody> <tr> <th>Termin</th> <td>Nd, 30 kwietnia, 12:00</td> </tr> <tr> <th>Czas trwania prelekcji</th> <td>50&nbsp;minut</td> </tr> <tr> <th>Sala/miejsce</th> <td>Literacka 2</td> </tr> <tr> <th>Kategoria</th> <td>Blok Literacki</td> </tr> <tr> <th>Opis</th> <td>Czego potrzeba, by zyskać moc? Wypadek w laboratorium, odziedziczona fortuna, promieniowanie z kosmosu? A może wystarczy się zaciągnąć? I ty możesz dołączyć do armii, poszerzyć granice znanego świata, zostać perfekcyjną maszyną do zabijania. Posłuchaj o modyfikacjach biologicznych, futurystycznym sprzęcie, magicznej broni, morderczym treningu, nienagannej dyscyplinie, braterstwie broni i wszystkich innych czynnikach składających się na fantastyczne wojsko doskonałe.</td> </tr> <tr> <th>Goście</th> <td> <ul> <li>Robert M. Wegner</li> </ul> </td> </tr> <tr> <th>Prowadzący</th> <td> <ul> <li>Maciej "Toudi" Pitala</li> <li>Marcin Przybyłek</li> </ul> </td> </tr> <tr> <th>Tagi</th> <td> <ul> <a href="/2017/program/?tag=przybylek_1" class="button button--small">Przybyłek</a>&nbsp; <a href="/2017/program/?tag=wegner_1" class="button button--small">Wegner</a>&nbsp; </ul> </td> </tr> </tbody> </table> </div> </div> <!-- <div class="program-point__action"> <a href="/2017/program/" title="" class="button button--icon-arrow-right button--icon-right">Wróć do programu</a> </div> --></div> </div> </div>';
  //   template10.parseEvent(1881, body, function (err, result) {
  //       console.log(result);
  //       result.should.have.deep.property('name', '[Panel] Podbić świat!');
  //       result.should.have.deep.property('description', 'Czego potrzeba, by zyskać moc? Wypadek w laboratorium, odziedziczona fortuna, promieniowanie z kosmosu? A może wystarczy się zaciągnąć? I ty możesz dołączyć do armii, poszerzyć granice znanego świata, zostać perfekcyjną maszyną do zabijania. Posłuchaj o modyfikacjach biologicznych, futurystycznym sprzęcie, magicznej broni, morderczym treningu, nienagannej dyscyplinie, braterstwie broni i wszystkich innych czynnikach składających się na fantastyczne wojsko doskonałe.');
  //       result.should.have.deep.property('duration.startAt', '2017-04-30T10:00:00.000Z');
  //       result.should.have.deep.property('duration.finishAt', '2017-04-30T10:50:00.000Z');
  //       result.should.have.deep.property('tags[0]', '1881');
  //       result.should.have.deep.property('tags[1]', 'Przybyłek');
  //       result.should.have.deep.property('tags[2]', 'Wegner');
  //       result.should.have.deep.property('authors[0].name', 'Robert M. Wegner');
  //       result.should.have.deep.property('authors[1].name', 'Maciej "Toudi" Pitala');
  //       result.should.have.deep.property('authors[2].name', 'Marcin Przybyłek');
  //       result.should.have.deep.property('categories[0].name', 'Blok Literacki');
  //       result.should.have.deep.property('places[0].name', 'Literacka 2');
  //
  //       done();
  //     }
  //   );
  // });
});